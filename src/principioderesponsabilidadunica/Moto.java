/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principioderesponsabilidadunica;

/**
 *
 * @author CompuStore
 */
public class Moto {
    public int getRuedas() {
        return 2;
    }
 
    public int getVelocidad() {
        return 180;
    }
    @Override public String toString() {
        return "Numero De Ruedas:   "   + getRuedas() +   "Velocidad Maxima:   " + getVelocidad();
    }
 
    public void print() {
        System.out.println(toString());
    }
}
